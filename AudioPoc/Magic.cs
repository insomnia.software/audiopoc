﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NAudio;
using NAudio.Lame;
using NAudio.Wave;

namespace AudioPoc
{
	class Magic
	{
		public void DoMagic()
		{
			while (true)
			{
				StartMic();
				StartSpeakers();
				while (!CheckFileSize())
				{
					Thread.Sleep(5000);
				}
				StopMic();
				StopSpeakers();
				flipFlop = !flipFlop;
				//Task.Run(() =>
				{
					Thread.Sleep(1000);
					WaveToMP3(GetOldMicFilePath(), @"C:\home\" + mp3Cout.ToString() + "m");
					WaveToMP3(GetOldSpeakerFilePath(), @"C:\home\" + mp3Cout.ToString() + "s");
				}//);
			}
		}

		int mp3Cout = 1;

		const string MIC_FILE_PATH = @"C:\home\mic.wav";
		const string SPEAKER_FILE_PATH = @"C:\home\speakers.wav";
		const string MIC_FILE_PATH1 = @"C:\home\mic1.wav";
		const string SPEAKER_FILE_PATH1 = @"C:\home\speakers1.wav";

		bool flipFlop = false;

		string GetOldMicFilePath()
		{
			if (!flipFlop)
				return MIC_FILE_PATH1;
			else
				return MIC_FILE_PATH;
		}

		string GetOldSpeakerFilePath()
		{
			if (!flipFlop)
				return SPEAKER_FILE_PATH1;
			else
				return SPEAKER_FILE_PATH;
		}

		string GetMicFilePath()
		{
			if (flipFlop)
				return MIC_FILE_PATH1;
			else
				return MIC_FILE_PATH;
		}

		string GetSpeakerFilePath()
		{
			if (flipFlop)
				return SPEAKER_FILE_PATH1;
			else
				return SPEAKER_FILE_PATH;
		}

		const int O = 10000000;

		bool CheckFileSize()
		{
			FileInfo fiMic = new FileInfo(GetMicFilePath());
			FileInfo fiSpeakers = new FileInfo(GetSpeakerFilePath());

			if (fiMic.Length > O || fiSpeakers.Length > O)
			{
				return true;
			}
			return false;
		}

		void waveSource_RecordingStopped(object sender, StoppedEventArgs e)
		{
			if (waveSourceMic != null)
			{
				waveSourceMic.Dispose();
				waveSourceMic = null;
			}

			if (waveFileMic != null)
			{
				waveFileMic.Dispose();
				waveFileMic = null;
			}
		}

		public WaveInEvent waveSourceMic = null;
		public WaveFileWriter waveFileMic = null;

		private void StartMic()
		{

			waveSourceMic = new WaveInEvent();
			waveSourceMic.WaveFormat = new WaveFormat(44100, 1);

			waveSourceMic.DataAvailable += new EventHandler<WaveInEventArgs>(waveSource_DataAvailable);
			waveSourceMic.RecordingStopped += new EventHandler<StoppedEventArgs>(waveSource_RecordingStopped);

			waveFileMic = new WaveFileWriter(GetMicFilePath(), waveSourceMic.WaveFormat);

			try
			{
				waveSourceMic.StartRecording();
			}
			catch (MmException exception)
			{
				//no mic
			}
		}

		private void StopMic()
		{
			try
			{
				waveSourceMic.StopRecording();
			}
			catch (MmException exception)
			{
				//no mic
			}
		}

		void waveSource_DataAvailable(object sender, WaveInEventArgs e)
		{
			if (waveFileMic != null)
			{
				waveFileMic.Write(e.Buffer, 0, e.BytesRecorded);
				waveFileMic.Flush();
			}
		}

		void waveSourceSpeakers_RecordingStopped(object sender, StoppedEventArgs e)
		{
			if (waveSourceSpeakers != null)
			{
				waveSourceSpeakers.Dispose();
				waveSourceSpeakers = null;
			}

			if (waveFileSpeakers != null)
			{
				waveFileSpeakers.Dispose();
				waveFileSpeakers = null;
			}
		}

		public static void WaveToMP3(string waveFileName, string mp3FileName, int bitRate = 128)
		{
			using (var reader = new WaveFileReader(waveFileName))
			using (var writer = new LameMP3FileWriter(mp3FileName, reader.WaveFormat, bitRate))
				reader.CopyTo(writer);
		}
		public WasapiLoopbackCapture waveSourceSpeakers = null;
		public WaveFileWriter waveFileSpeakers = null;

		private void StartSpeakers()
		{

			waveSourceSpeakers = new WasapiLoopbackCapture();
			//waveSourceSpeakers.WaveFormat = new WaveFormat(44100, 1);

			waveSourceSpeakers.DataAvailable += new EventHandler<WaveInEventArgs>(waveSourceSpeakers_DataAvailable);
			waveSourceSpeakers.RecordingStopped += new EventHandler<StoppedEventArgs>(waveSourceSpeakers_RecordingStopped);

			waveFileSpeakers = new WaveFileWriter(GetSpeakerFilePath(), waveSourceSpeakers.WaveFormat);

			waveSourceSpeakers.StartRecording();
		}

		private void StopSpeakers()
		{
			waveSourceSpeakers.StopRecording();
		}

		void waveSourceSpeakers_DataAvailable(object sender, WaveInEventArgs e)
		{
			if (waveFileSpeakers != null)
			{
				waveFileSpeakers.Write(e.Buffer, 0, e.BytesRecorded);
				waveFileSpeakers.Flush();
			}
		}

	}
}
